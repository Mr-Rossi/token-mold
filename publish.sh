if [[ $# -eq 0 ]]
	then
		echo "Please supply type of update."
		echo "(valid types: hotfix, major, minor)"
		exit 1
fi
if [[ $1 == 'hotfix' ]]
	then npm run hotfix
elif [[ $1 == 'major' ]]
	then npm run major
elif [[ $1 == 'minor' ]]
	then npm run minor
else
	echo 'Invalid update type.'
	echo "(valid types: hotfix, major, minor)"
fi

echo 'zipping release'
npm run package
echo 'commiting and tagging'

echo 'commiting changes to dev'
git commit --all -m "v$(eval jq -r '.version' module.json)"
git push

git checkout master
git merge --strategy-option=theirs --squash dev

git add dist/*
git commit --all -F doc/changelog.txt
git tag -a "v$(eval jq -r '.version' module.json)" -F doc/changelog.txt
git push && git push --tags
git push
git checkout dev
