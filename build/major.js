const fs = require('fs');
const path = require('path');
const util = require('./util');
const moduleJson = util.readModuleJson();

let v = moduleJson.version.split(".");
v[0] = Number(v[0])+1
moduleJson.version = v[0] + '.0';

fs.writeFileSync(path.join(util.getBaseDir(), "module.json"), JSON.stringify(moduleJson, null, 2));
util.setChangelogVersion(`Release v${moduleJson.version}`)


util.updateUrl();